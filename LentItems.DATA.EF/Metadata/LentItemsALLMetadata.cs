﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
//gives access to meta data for validayion, 0display, displayformat, uihints, and that other thing.

namespace LentItems.DATA.EF//.Metadata
{
    #region Metadata rules
    /*
    1- metadata class and the originial class genreated from tt file must live in the same namespace and must be public

    2- partial class name must match exactly (spelling and casing) the slass declared by the tt file

    3- create a metadata class and that is wehre we define all of the properties (declared in the original class) and add our metadata attributes.

    4-using the metadatatype attrivute applyu our metadata class to the publix partial class

    */
    #endregion


    #region useless junk
    //class LentItemsALLMetadata
    //{free generated class genereated by visual studio when creating a file
    //}
    //this file will hold all metadata references for our tables that belong to the LentItems DB

    #endregion

    #region Category information

    public class CategoryMetaData
    {
        //public int CategoryID { get; set; }

        [Required(ErrorMessage = "category name is required")]//thiis table field is non nullable
        [Display(Name = "Category")]
        [StringLength(30, ErrorMessage = "this field is limited to 30 characters")]
        public string CategoryName { get; set; }

        [UIHint("MultilineText")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [StringLength(250, ErrorMessage = "can only accept a 250 character description")]
        public string Description { get; set; }


    }

    [MetadataType(typeof(CategoryMetaData))]
    public partial class Category
    {

    }


    #endregion


    #region item information

    public class ItemMetaData
    {
        //public int ItemID { get; set; }

        [Required(ErrorMessage = "name is required")]
        [Display(Name = "Item")]
        [StringLength(50, ErrorMessage = "this field is limited to 50 characters")]

        public string ItemName { get; set; }

        [Required(ErrorMessage = "Category is required")]
        [Display(Name = "category id")]

        public int CategoryID { get; set; }

        [UIHint("MultilineText")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [StringLength(250, ErrorMessage = "can only accept a 250 character description")]

        public string Description { get; set; }
    }

    [MetadataType(typeof(ItemMetaData))]
    public partial class Item
    {
    }

    #endregion


    #region lentitem information
    public class LentItemMetadata
    {
        //public int LentItemID { get; set; }
        //public int ItemID { get; set; }

[Required]
        public string BorrowerFName { get; set; }
        [Required]
        public string BorrowerLName { get; set; }
        [Required]
        public System.DateTime DateLent { get; set; }
        public Nullable<System.DateTime> DateReturned { get; set; }

    }


    #endregion

}
